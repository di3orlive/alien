jQuery.exists = function (selector) {
    return ($(selector).length > 0);
};


(function() {
    $(document).ready(function() {

        if($.exists(".open-modal")) {
            $('.open-modal').click(function (e) {
                e.preventDefault();
                var href = $(this).attr('href');

                if ($(href).hasClass('active-modal')) {
                    $(href).removeClass('active-modal');
                    $("html,body").css("overflow","initial");
                }else{
                    $(href).addClass('active-modal');
                    $("html,body").css("overflow","hidden");
                }
            });
        }


        if($.exists(".modal-content")) {
            $(".modal-content").mCustomScrollbar();
        }





    });
}).call(this);


